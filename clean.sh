#!/bin/bash

set -e

cd buildroot-2019.05
make clean

cd ../
exec ./build.sh
