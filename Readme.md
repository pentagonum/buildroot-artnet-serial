# Buildroot-Artnet-Serial

Buildroot for OrangePie Zero and [Artnet-Serial](https://gitlab.com/pentagonum/artnet-serial).

Static IP is set in ip.txt

Boots in 4 seconds and is very stable.
