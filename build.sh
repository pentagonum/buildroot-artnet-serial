#!/bin/bash
set -e

cd buildroot-2019.05
make BR2_EXTERNAL=../artnet-serial orangepi_zero_defconfig
make
cp output/images/sdcard.img ../
