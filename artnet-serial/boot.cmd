setenv bootargs console=${console} root=/dev/ram0

fatload mmc 0 $kernel_addr_r zImage
fatload mmc 0 $fdt_addr_r sun8i-h2-plus-orangepi-zero.dtb

bootz ${kernel_addr_r} - ${fdt_addr_r};

