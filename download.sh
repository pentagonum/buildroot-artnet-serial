#!/bin/bash

set -e

wget https://buildroot.org/downloads/buildroot-2019.05.tar.gz
tar xvfz buildroot-2019.05.tar.gz
rm -f buildroot-2019.05.tar.gz
